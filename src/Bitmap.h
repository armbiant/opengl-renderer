﻿#pragma once

#include <cstdint>
#include <cstring>
#include <vector>

enum BitmapType { BitmapType_2D, BitmapType_Cube };

enum BitmapFormat {
  BitmapFormat_UnsignedByte,
  BitmapFormat_Float,
};

struct Bitmap {
  Bitmap(int width, int height, int depth, int comp, BitmapFormat format)
      : width(width), height(height), depth(depth), components(comp),
        format(format),
        data(static_cast<size_t>(width * height * depth * comp *
                                 getBytesPerComponent(format))) {}
  Bitmap(int width, int height, int comp, BitmapFormat format, const void *ptr)
      : width(width), height(height), components(comp), format(format),
        data(static_cast<size_t>(width * height * comp *
                                 getBytesPerComponent(format))) {
    memcpy(data.data(), ptr, data.size());
  }

  int width = 0;
  int height = 0;
  int depth = 1;
  int components = 3;
  BitmapFormat format = BitmapFormat_UnsignedByte;
  BitmapType type = BitmapType_2D;
  std::vector<uint8_t> data;

  inline static auto getBytesPerComponent(BitmapFormat format) -> int {
    if (format == BitmapFormat_UnsignedByte) {
      return 1;
    }
    if (format == BitmapFormat_Float) {
      return 4;
    }
    return 0;
  }
};
