#!/usr/bin/env bash
if [[ "$#" -ne 2 ]]; then
  echo "Usage: 2-setup-project.sh [toolchain] [mode]"
  echo "Toolchains: clang, gcc (xmake show -l toolchains)"
  echo "Modes: debug, undefined, memory, thread, valgrind, profile, release"
  exit 1
fi
export CONAN_CPU_COUNT=$(($(nproc)+2)) # https://docs.conan.io/en/1.6/reference/config_files/conan.conf.html
rm -rf build/ .xmake/ compile_commands.json
xmake f --toolchain=$1 -y -m $2
xmake project -k compile_commands
