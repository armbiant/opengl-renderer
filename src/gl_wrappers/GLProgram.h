#pragma once
#include "GLShader.h"

class GLProgram {
public:
  GLProgram(const GLShader &one, const GLShader &two);
  ~GLProgram();

  auto operator=(const GLProgram &) -> GLProgram & = delete;
  GLProgram(const GLProgram &) = delete;
  auto operator=(GLProgram &&) -> GLProgram & = delete;
  GLProgram(GLProgram &&other) noexcept;

  auto useProgram() const -> void;
  [[nodiscard]] auto get_handle() const -> GLuint { return handle; }

private:
  GLuint handle;
};
