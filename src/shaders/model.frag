#version 460 core

#include <src/shaders/common.all>

layout (location=0) in WorldVertexData world_vertex_data;
layout (location=0) out vec4 out_FragColor;

void main() {
  vec3 world_normal = normalize(world_vertex_data.normal_vector);
  vec3 world_view = normalize(camera_position.xyz - world_vertex_data.position);
  vec3 world_reflection = -normalize(reflect(world_view, world_normal));

  // Use Schlick's approximation to compute the specular reflection coefficient Rtheta
  float eta = 1.00 / 1.31; // ice
  vec3 refraction = -normalize(refract(world_view, world_normal, eta));

  const float R0 = ((1.0-eta) * (1.0-eta)) / ((1.0+eta) * (1.0+eta));
  const float Rtheta = R0 + (1.0 - R0) * pow((1.0 - dot(-world_view, world_normal)), 5.0);

  // Sample diffuse texture
  vec4 diffuse_color = texture(model_texture, world_vertex_data.uv_coords);

  // Sample the cubemap environment
  vec4 reflection_color = texture(cubemap_texture, world_reflection);
  vec4 refraction_color = texture(cubemap_texture, refraction);
  out_FragColor = diffuse_color * mix(reflection_color, refraction_color, Rtheta);
};
