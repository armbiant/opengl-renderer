#!/usr/bin/env bash

sudo pacman -S gcc gdb clang cmake ninja watchexec lldb valgrind llvm # llvm for llvm-symbolizer
pikaur -S xmake conan

# sdl dependencies
# sudo pacman -S libfontenc libice libsm libxaw libxcomposite libxcursor libxtst libxinerama libxkbfile libxrandr libxres libxss libxvmc xcb-util-image xcb-util-keysyms libxv xcb-util

# glfw dependencies
sudo pacman -S libfontenc libxaw libxkbfile libxres libxss libxvmc xcb-util-wm xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util libglvnd libxcomposite libxcursor libxtst libxinerama libxrandr libxv
