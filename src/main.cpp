#include "Bitmap.h"
#include "gl_wrappers/GLProgram.h"
#include "gl_wrappers/GLShader.h"
#include "debug.h"
#include "glstructs.h"
#include "utils.h"
#include <GLFW/glfw3.h>

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <cstddef>
#include <cstdint>
#include <fmt/core.h>
#include <span>
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <gsl/gsl>
#include <stb_image_write.h>
#include <tuple>
#include <vector>

/**
 * GLFW window is created, GLAD function pointers are loaded and opengl settings
 * are set.
 *
 * @return created GLFWwindow
 */
auto create_glfw_window() -> gsl::owner<GLFWwindow *> {
  // Create GLFW window
  glfwSetErrorCallback([](int error, const char *description) {
    fmt::print("Error {}: {}\n", error, description);
  });

  if (glfwInit() == 0) {
    std::quick_exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  if constexpr (glstructs::DEBUG_ENABLED) {
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
  }
  auto *window = gsl::owner<GLFWwindow *>(
      glfwCreateWindow(glstructs::RES_WIDTH, glstructs::RES_HEIGHT,
                       glstructs::WINDOW_TITLE, nullptr, nullptr));
  if (window == nullptr) {
    glfwTerminate();
    std::quick_exit(EXIT_FAILURE);
  }

  glfwSetKeyCallback(window, [](GLFWwindow *window, int key, int /*scancode*/,
                                int action, int /*mods*/) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
      glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
  });
  glfwMakeContextCurrent(window);

  // Load GLAD function pointers
  gladLoadGL(glfwGetProcAddress);
  if constexpr (glstructs::DEBUG_ENABLED) {
    debug::init_debug();
  }

  // Set opengl settings
  glfwSwapInterval(1);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glClearColor(1.0F, 1.0F, 1.0F, 1.0F);
  return window;
}

/**
 * Shader sources are loaded from file, compiled and linked into a GLProgram.
 *
 * @return Tuple of cubemap program and model program
 */
auto create_programs() -> std::tuple<GLProgram, GLProgram> {
  const GLShader model_vertex_shader{"src/shaders/model.vert"};
  const GLShader model_frag_shader{"src/shaders/model.frag"};
  GLProgram model_program{model_vertex_shader, model_frag_shader};

  const GLShader cubemap_vertex_shader{"src/shaders/cube.vert"};
  const GLShader cubemap_frag_shader{"src/shaders/cube.frag"};
  GLProgram cubemap_program{cubemap_vertex_shader, cubemap_frag_shader};

  return {std::move(cubemap_program), std::move(model_program)};
}

/**
 * Empty perframedata uniform buffer is created and binded to uniform buffer
 * location 0.
 *
 * @return GLuint handle of perframedata uniform buffer
 */
auto create_perframedata_uniform_buffer() -> GLuint {
  GLuint perframedata_uniform_buffer = 0;
  glCreateBuffers(1, &perframedata_uniform_buffer);
  glNamedBufferStorage(perframedata_uniform_buffer,
                       sizeof(glstructs::PerFrameData), nullptr,
                       GL_DYNAMIC_STORAGE_BIT);
  glBindBufferRange(GL_UNIFORM_BUFFER, 0, perframedata_uniform_buffer, 0,
                    sizeof(glstructs::PerFrameData));
  return perframedata_uniform_buffer;
}

/**
 * Vertex data and indices data are loaded from a scene file using assimp into
 * vectors.
 *
 * @return Tuple of vertices data vector and indices data vector
 */
auto create_vertices_and_indices_vectors_from_scene()
    -> std::tuple<std::vector<glstructs::LocalVertexData>,
                  std::vector<unsigned int>> {
  const auto *scene =
      aiImportFile("data/rubber_duck/scene.gltf", aiProcess_Triangulate);
  if (scene == nullptr || !scene->HasMeshes()) {
    fmt::print("Unable to load data/rubber_duck/scene.gltf\n");
    std::quick_exit(EXIT_FAILURE);
  }

  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  const auto *mesh = scene->mMeshes[0];

  std::vector<glstructs::LocalVertexData> local_vertices_data{};
  for (unsigned i = 0; i < mesh->mNumVertices; i++) {
    const std::span<aiVector3D> vertex_span{mesh->mVertices,
                                            mesh->mNumVertices};
    const std::span<aiVector3D> normal_span{mesh->mNormals, mesh->mNumVertices};
    const std::span<aiVector3D> tex_coords_span{mesh->mTextureCoords[0],
                                                mesh->mNumVertices};

    const auto vertex_position = vertex_span[i];
    const auto normal_vector = normal_span[i];
    const auto tex_coords = tex_coords_span[i];

    local_vertices_data.push_back(
        {.position =
             vec3(vertex_position.x, vertex_position.z, vertex_position.y),
         .normal_vector =
             vec3(normal_vector.x, normal_vector.y, normal_vector.z),
         .tex_coords = vec2(tex_coords.x, tex_coords.y)});
  }

  std::vector<unsigned int> indices_data{};
  const std::span<aiFace> face_span{mesh->mFaces, mesh->mNumFaces};
  for (const auto &face : face_span) {
    const std::span<unsigned int> index_span{face.mIndices, 3};
    for (const auto &index : index_span) {
      indices_data.push_back(index);
    }
  }
  aiReleaseImport(scene);
  return {local_vertices_data, indices_data};
}

/**
 * Immutable buffer of indices data is created using indices data vector.
 *
 * @param indices_data The indices data vector
 * @return GLuint handle of immutable indices buffer
 */
auto create_indices_buffer(const std::vector<unsigned int> &indices_data)
    -> GLuint {
  GLuint indices_buffer = 0;
  glCreateBuffers(1, &indices_buffer);
  glNamedBufferStorage(indices_buffer, utils::sizeof_vector_data(indices_data),
                       indices_data.data(), 0);
  return indices_buffer;
}

/**
 * Vertex array object is created and binded to global OpenGL context. The
 * indices buffer is bound to the vertex array object.
 *
 * @param indices_buffer GLuint handle of indices buffer
 * @return GLuint handle of vertex array object
 */
auto create_and_bind_vao(const GLuint indices_buffer) -> GLuint {
  GLuint vao = 0;
  glCreateVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glVertexArrayElementBuffer(vao, indices_buffer);
  return vao;
}

/**
 * Immutable buffer of vertex data is created using vertex data vector and bound
 * to shader storage buffer location 0.
 *
 * @param local_vertices_data The vertices data vector
 * @return GLuint handle of immutable vertices buffer
 */
auto create_vertices_buffer(
    const std::vector<glstructs::LocalVertexData> &local_vertices_data)
    -> GLuint {
  GLuint vertices_buffer = 0;
  glCreateBuffers(1, &vertices_buffer);
  glNamedBufferStorage(vertices_buffer,
                       utils::sizeof_vector_data(local_vertices_data),
                       local_vertices_data.data(), 0);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vertices_buffer);
  return vertices_buffer;
}

/**
 * Immutable model texture is created using a texture file and bound to texture
 * location 0.
 */
auto create_model_texture() -> void {
  GLuint texture = 0;

  int width = 0;
  int height = 0;
  int components = 0;
  auto *const img = gsl::owner<stbi_uc *>(
      stbi_load("data/rubber_duck/textures/Duck_baseColor.png", &width, &height,
                &components, 3));

  glCreateTextures(GL_TEXTURE_2D, 1, &texture);
  glTextureParameteri(texture, GL_TEXTURE_MAX_LEVEL, 0);
  glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTextureStorage2D(texture, 1, GL_RGB8, width, height);
  glTextureSubImage2D(texture, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE,
                      img);
  glBindTextures(0, 1, &texture);

  stbi_image_free(static_cast<void *>(img));
}

/**
 * Immutable cubemap environment texture is created using a texture file and
 * bound to texture location 1.
 */
auto create_cubemap_texture() -> void {
  GLuint cubemapTex = 0;

  int width = 0;
  int height = 0;
  int components = 0;
  const float *img =
      stbi_loadf("data/cubemap.hdr", &width, &height, &components, 3);
  const Bitmap texture(width, height, components, BitmapFormat_Float, img);
  stbi_image_free((void *)img);

  Bitmap cubemap = utils::convertVerticalCrossToCubeMapFaces(texture);

  glCreateTextures(GL_TEXTURE_CUBE_MAP, 1, &cubemapTex);
  glTextureParameteri(cubemapTex, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTextureParameteri(cubemapTex, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTextureParameteri(cubemapTex, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glTextureParameteri(cubemapTex, GL_TEXTURE_BASE_LEVEL, 0);
  glTextureParameteri(cubemapTex, GL_TEXTURE_MAX_LEVEL, 0);
  glTextureParameteri(cubemapTex, GL_TEXTURE_MAX_LEVEL, 0);
  glTextureParameteri(cubemapTex, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTextureParameteri(cubemapTex, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTextureStorage2D(cubemapTex, 1, GL_RGB32F, cubemap.width, cubemap.height);

  int index = 0;
  for (int i = 0; i < 6; i++) {
    glTextureSubImage3D(cubemapTex, 0, 0, 0, i, cubemap.width, cubemap.height,
                        1, GL_RGB, GL_FLOAT, &cubemap.data[index]);
    index += cubemap.width * cubemap.height * cubemap.components *
             Bitmap::getBytesPerComponent(cubemap.format);
  }
  glBindTextures(1, 1, &cubemapTex);
}

/**
 * Renders model using perspective matrix and model program as input.
 * perframedata_uniform_buffer is updated for the model program.
 *
 * @param perspective_matrix The perspective matrix (constant per frame)
 * @param model_program The model program
 * @param perframedata_uniform_buffer The perframedata uniform buffer for the
 * model program
 * @param INDICES_SIZE Constant for number of indices. Used for glDrawElements.
 */
auto render_model(const mat4 perspective_matrix, const GLProgram &model_program,
                  const GLuint perframedata_uniform_buffer,
                  const GLsizei INDICES_SIZE) -> void {
  const mat4 model_matrix =
      glm::rotate(glm::translate(mat4(1.0F), vec3(0.0F, -0.5F, -1.5F)),
                  static_cast<float>(glfwGetTime()), vec3(0.0F, 1.0F, 0.0F));

  const glstructs::PerFrameData perframedata = {.model_matrix = model_matrix,
                                              .mvp_matrix = perspective_matrix *
                                                            model_matrix,
                                              .camera_position = vec4(0.0F)};
  glNamedBufferSubData(perframedata_uniform_buffer, 0,
                       sizeof(glstructs::PerFrameData), &perframedata);
  model_program.useProgram();
  glDrawElements(GL_TRIANGLES, INDICES_SIZE, GL_UNSIGNED_INT, nullptr);
}

/**
 * Renders cubemap environment using perspective matrix and cubemap environment
 * program as input. perframedata_uniform_buffer is updated for the cubemap
 * environment program.
 *
 * @param perspective_matrix The perspective matrix (constant per frame)
 * @param cubemap environment_program The cubemap environment program
 * @param perframedata_uniform_buffer The perframedata uniform buffer for the
 * cubemap environment program
 */
auto render_cubemap(const mat4 perspective_matrix,
                    const GLProgram &cubemap_program,
                    const GLuint perframedata_uniform_buffer) -> void {
  const mat4 model_matrix = glm::scale(mat4(1.0F), vec3(2.0F));
  const glstructs::PerFrameData perframedata = {.model_matrix = model_matrix,
                                              .mvp_matrix = perspective_matrix *
                                                            model_matrix,
                                              .camera_position = vec4(0.0F)};
  glNamedBufferSubData(perframedata_uniform_buffer, 0,
                       sizeof(glstructs::PerFrameData), &perframedata);
  cubemap_program.useProgram();
  glDrawArrays(GL_TRIANGLES, 0, 36);
}

/**
 * Cleanup OpenGL objects and exit.
 *
 * @param indices_buffer Indices buffer object
 * @param vertices_buffer Vertives buffer object
 * @param perframedata_uniform_buffer Perdataframe uniform buffer object
 * @param vao VAO object
 * @param window GLFW window
 */
void delete_objects_and_exit(const GLuint indices_buffer,
                             const GLuint vertices_buffer,
                             const GLuint perframedata_uniform_buffer,
                             const GLuint vao,
                             const gsl::owner<GLFWwindow *> window) {
  glDeleteBuffers(1, &indices_buffer);
  glDeleteBuffers(1, &vertices_buffer);
  glDeleteBuffers(1, &perframedata_uniform_buffer);
  glDeleteVertexArrays(1, &vao);

  glfwDestroyWindow(window);
  glfwTerminate();
}

auto main() -> int {
  const gsl::owner<GLFWwindow *> window = create_glfw_window();
  const auto [cubemap_program, model_program] = create_programs();
  const auto perframedata_uniform_buffer = create_perframedata_uniform_buffer();

  const auto [local_vertices_data, indices_data] =
      create_vertices_and_indices_vectors_from_scene();
  const auto indices_immutable_buffer = create_indices_buffer(indices_data);
  const auto vao = create_and_bind_vao(indices_immutable_buffer);
  const auto vertices_immutable_buffer =
      create_vertices_buffer(local_vertices_data);

  create_model_texture();
  create_cubemap_texture();

  while (glfwWindowShouldClose(window) == 0) {
    // Compute perspective matrix
    int width = 0;
    int height = 0;
    glfwGetFramebufferSize(window, &width, &height);
    const auto ratio = static_cast<float>(width) / static_cast<float>(height);
    const auto perspective_matrix =
        glm::perspective(45.0F, ratio, 0.1F, 1000.0F);

    // Draw
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    render_model(perspective_matrix, model_program, perframedata_uniform_buffer,
                 static_cast<GLsizei>(indices_data.size()));
    render_cubemap(perspective_matrix, cubemap_program,
                   perframedata_uniform_buffer);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  delete_objects_and_exit(indices_immutable_buffer, vertices_immutable_buffer,
                          perframedata_uniform_buffer, vao, window);
}
