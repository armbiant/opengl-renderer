﻿#pragma once

#include <glad/gl.h>
#include <string>

class GLShader {
public:
  GLShader(const std::string &file_name);
  GLShader(GLenum type, std::string source_code,
           const std::string &file_name = "");
  ~GLShader();

  auto operator=(const GLShader &) -> GLShader & = default;
  GLShader(const GLShader &) = default;
  auto operator=(GLShader &&) -> GLShader & = delete;
  GLShader(const GLShader &&) = delete;

  [[nodiscard]] auto get_handle() const -> GLuint { return handle; }

private:
  GLenum type;
  GLuint handle;
  static auto detect_shader_type(const std::string &file_name) -> GLenum;
  static auto remove_last_char(std::string string) -> std::string;
  static auto read_shader_file(const std::string &file_name) -> std::string;
};
