﻿#include "GLShader.h"

#include <boost/algorithm/string.hpp>
#include <fmt/core.h>
#include <fstream>
#include <vector>

GLShader::GLShader(const std::string &file_name)
    : GLShader(detect_shader_type(file_name), read_shader_file(file_name),
               file_name) {}

GLShader::GLShader(GLenum type, std::string source_code,
                   const std::string &file_name)
    : type(type), handle(glCreateShader(type)) {
  if (source_code[0] != '#') {
    fmt::print("first character of shader source needs to be '#'. check shader "
               "source encoding (no BOM) {}\n",
               file_name);
    std::quick_exit(1);
  }

  const char *source_code_char_pointer = source_code.c_str();
  glShaderSource(handle, 1, &source_code_char_pointer, nullptr);
  glCompileShader(handle);

  std::array<char, 8192> buffer{};
  GLsizei length = 0;
  glGetShaderInfoLog(handle, buffer.size(), &length, buffer.data());

  if (length != 0) {
    fmt::print("{} {}\n", file_name, buffer.data());
    std::quick_exit(-1);
  }
}

GLShader::~GLShader() { glDeleteShader(handle); }

auto GLShader::detect_shader_type(const std::string &file_name) -> GLenum {
  if (file_name.ends_with(".vert")) {
    return GL_VERTEX_SHADER;
  }
  if (file_name.ends_with(".frag")) {
    return GL_FRAGMENT_SHADER;
  }
  if (file_name.ends_with(".geom")) {
    return GL_GEOMETRY_SHADER;
  }
  if (file_name.ends_with(".tesc")) {
    return GL_TESS_CONTROL_SHADER;
  }
  if (file_name.ends_with(".tese")) {
    return GL_TESS_EVALUATION_SHADER;
  }
  if (file_name.ends_with(".comp")) {
    return GL_COMPUTE_SHADER;
  }
  fmt::print("Failed to detect shader type\n");
  std::quick_exit(-1);
  return 0;
}

auto GLShader::remove_last_char(std::string string) -> std::string {
  if (!string.empty()) {
    string.pop_back();
  }
  return string;
}

auto GLShader::read_shader_file(const std::string &file_name) -> std::string {
  using std::fstream;

  fstream file_stream{file_name, fstream::in};
  if (!file_stream.is_open()) {
    fmt::print("failed to open {}\n", file_name);
    std::quick_exit(-1);
  }

  std::string source_code{};
  std::string source_line{};
  while (std::getline(file_stream, source_line)) {
    if (source_line.starts_with("#include <")) {
      std::vector<std::string> delimited_strings;
      boost::split(delimited_strings, source_line, boost::is_any_of("<"));
      source_line = read_shader_file(remove_last_char(delimited_strings.at(1)));
    }
    source_code.append(source_line);
    source_code.append("\n");
  }
  return source_code;
}
