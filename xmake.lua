set_languages("cxxlatest") -- use latest C++ standard
set_warnings("all", "error") -- enable all warnings and error out on warning
add_cxxflags("clang::-stdlib=libstdc++") -- if compiling with clang, use GCC's libstdc++ since target is linux and libstdc++ is ABI incompatible with libc++
add_cxxflags("-ferror-limit=0") -- do not stop showing errors if there are too many errors

-- Modes
add_rules("mode.valgrind") -- add valgrind mode
if is_mode("debug") or is_mode("undefined") or is_mode("memory") or is_mode("thread") then
  set_symbols("debug")
  set_optimize("none")
  add_cxxflags("-D_GLIBCXX_DEBUG") -- enable libstdc++ debug mode (e.g. array bounds checking)
end
if is_mode("undefined") then -- AddressSanitizer (detects addressability issues) + LeakSanitizer (detects memory leaks) + UndefinedBehaviorSanitizer
  add_cxxflags("-fsanitize=address,leak,undefined")
  add_ldflags("-fsanitize=address,leak,undefined")
end
if is_mode("memory") then -- MemorySanitizer (detects use of uninitialized memory). Unsupported by GCC (use valgrind). All libraries must be compiled with these flags too.
  add_cxxflags("-fsanitize=memory")
  add_ldflags("-fsanitize=memory")
end
if is_mode("thread") then -- ThreadSanitizer (detects data races and deadlocks)
  add_cxxflags("-fsanitize=thread")
  add_ldflags("-fsanitize=thread")
end
if is_mode("profile") then
  set_symbols("debug")
  set_optimize("fastest")
  add_cxxflags("-pg")
  add_ldflags("-pg")
end
if is_mode("release") then
  set_symbols("hidden")
  set_optimize("fastest")
  set_strip("all")
end

-- Dependent packages
-- glad2 configured with: gl4.6 core and vulkan 1.2
add_requires("conan::glfw/3.3.8", {alias = "glfw"})
add_requires("conan::glm/0.9.9.8", {alias = "glm"})
add_requires("conan::assimp/5.2.2", {alias = "assimp"}) -- comes with stb
add_requires("conan::fmt/9.1.0", {
                                     alias = "fmt",
                                     configs = {
                                       options = {
                                         "fmt:header_only=True",
                                       }
                                     }
                                   })
add_requires("conan::ms-gsl/4.0.0", {alias = "gsl"})
add_requires("conan::boost/1.81.0", {alias = "boost"})
-- add_requires("conan::stb/cci.20220909", {alias = "stb"})
-- add_requires("conan::imgui/1.89.1", {alias = "imgui"})
-- add_requires("conan::easy_profiler/2.1.0", {alias = "easy_profiler"})
-- add_requires("conan::vulkan-headers/1.3.236.0", {alias = "vulkan-headers"})
-- add_requires("conan::sdl/2.26.1", {alias = "sdl2"})
-- add_requires("conan::tinyobjloader/2.0.0-rc10", {alias = "tol"})
-- add_requires("conan::vk-bootstrap/0.6", {alias = "vkbootstrap"})
-- add_requires("conan::vulkan-memory-allocator/3.0.1", {alias = "vma"})

-- Target configuration
target("opengl-renderer")
set_kind("binary")
add_files("src/**.cpp", "lib/glad2/*.c")
add_includedirs("include/glad2")
add_packages("glfw", "glm", "assimp", "fmt", "gsl", "boost")
