#include "GLProgram.h"
#include <fmt/core.h>
#include <utility>
#include <array>

GLProgram::GLProgram(const GLShader &one, const GLShader &two)
    : handle(glCreateProgram()) {
  glAttachShader(handle, one.get_handle());
  glAttachShader(handle, two.get_handle());
  glLinkProgram(handle);

  std::array<char, 8192> buffer{};
  GLsizei length = 0;
  glGetProgramInfoLog(handle, buffer.size(), &length, buffer.data());
  if (length != 0) {
    fmt::print("{}\n", buffer.data());
    std::quick_exit(-1);
  }

  glDetachShader(handle, one.get_handle());
  glDetachShader(handle, two.get_handle());
}

GLProgram::~GLProgram() { glDeleteProgram(handle); }

GLProgram::GLProgram(GLProgram &&other) noexcept
    : handle(std::exchange(other.handle, 0)) {}

auto GLProgram::useProgram() const -> void { glUseProgram(handle); }
