#version 460 core

#include <src/shaders/common.all>
#include <src/shaders/common.vert>

layout (location=0) out WorldVertexData world_vertex_data;

void main() {
  // set gl_Position = clip space position
  vec3 local_position = get_position_at_vid(gl_VertexID);
  gl_Position = mvp_matrix * vec4(local_position, 1.0);

  // normal matrix is mulitiplied to normal vector in case of non-uniform scaling
  mat3 normal_matrix = mat3(transpose(inverse(model_matrix)));

  // compute world space values
  world_vertex_data.uv_coords = get_tex_coords_at_vid(gl_VertexID);
  world_vertex_data.normal_vector = get_normal_vector_at_vid(gl_VertexID) * normal_matrix;
  world_vertex_data.position = (model_matrix * vec4(local_position, 1.0)).xyz;
}
