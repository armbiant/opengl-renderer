#pragma once
#include "Bitmap.h"
#include <glad/gl.h>
#include <vector>

namespace utils {
// Returns size of std::vector<T>'s data in bytes
template <typename T>
inline auto sizeof_vector_data(const typename std::vector<T> &vec)
    -> GLsizeiptr {
  return static_cast<GLsizeiptr>(sizeof(T) * vec.size());
}

/***
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
inline auto convertVerticalCrossToCubeMapFaces(const Bitmap &bitmap) -> Bitmap {
  const int faceWidth = bitmap.width / 3;
  const int faceHeight = bitmap.height / 4;

  Bitmap cubemap(faceWidth, faceHeight, 6, bitmap.components, bitmap.format);
  cubemap.type = BitmapType_Cube;

  const uint8_t *source = bitmap.data.data();
  uint8_t *destination = cubemap.data.data();

  /*
  ------
  | +Y |
  ----------------
  | -X | -Z | +X |
  ----------------
  | -Y |
  ------
  | +Z |
  ------
  */

  const int pixelSize =
      cubemap.components * Bitmap::getBytesPerComponent(cubemap.format);

  for (int face = 0; face != 6; ++face) {
    for (int j = 0; j != faceHeight; ++j) {
      for (int i = 0; i != faceWidth; ++i) {
        // NOLINTNEXTLINE(readability-identifier-length)
        int x = 0;
        // NOLINTNEXTLINE(readability-identifier-length)
        int y = 0;

        switch (face) {
        // GL_TEXTURE_CUBE_MAP_POSITIVE_X
        case 0:
          x = i;
          y = faceHeight + j;
          break;

        // GL_TEXTURE_CUBE_MAP_NEGATIVE_X
        case 1:
          x = 2 * faceWidth + i;
          y = 1 * faceHeight + j;
          break;

        // GL_TEXTURE_CUBE_MAP_POSITIVE_Y
        case 2:
          x = 2 * faceWidth - (i + 1);
          y = 1 * faceHeight - (j + 1);
          break;

        // GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
        case 3:
          x = 2 * faceWidth - (i + 1);
          y = 3 * faceHeight - (j + 1);
          break;

        // GL_TEXTURE_CUBE_MAP_POSITIVE_Z
        case 4:
          x = 2 * faceWidth - (i + 1);
          y = bitmap.height - (j + 1);
          break;

        // GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
        case 5:
          x = faceWidth + i;
          y = faceHeight + j;
          break;
        }

        // NOLINTNEXTLINE(bugprone-implicit-widening-of-multiplication-result,cppcoreguidelines-pro-bounds-pointer-arithmetic)
        memcpy(destination, source + (y * bitmap.width + x) * pixelSize,
               pixelSize);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        destination += pixelSize;
      }
    }
  }
  return cubemap;
}
} // namespace utils
