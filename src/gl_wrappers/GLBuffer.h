#pragma once
#include <glad/gl.h>

class GLBuffer {
public:
  GLBuffer(GLsizeiptr size, const void *data, GLbitfield flags);
  ~GLBuffer();

  auto operator=(const GLBuffer &) -> GLBuffer & = default;
  GLBuffer(const GLBuffer &) = default;
  auto operator=(GLBuffer &&) -> GLBuffer & = default;
  GLBuffer(const GLBuffer &&) = delete;

  [[nodiscard]] auto getHandle() const -> GLuint { return handle; }

private:
  GLuint handle;
};
