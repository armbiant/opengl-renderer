#version 460 core

#include <src/shaders/common.all>

layout (location=0) in vec3 direction_vector;
layout (location=0) out vec4 out_FragColor;

void main() {
  out_FragColor = texture(cubemap_texture, direction_vector);
};
