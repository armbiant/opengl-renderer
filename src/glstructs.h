#pragma once
#include <glad/gl.h>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

using glm::ivec2;
using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;

namespace glstructs {
constexpr auto DEBUG_ENABLED = true;
constexpr auto RES_WIDTH = 1280;
constexpr auto RES_HEIGHT = 720;
constexpr auto WINDOW_TITLE = "OpenGL Renderer";

// Data that is constant for a frame
struct PerFrameData {
  mat4 model_matrix;
  mat4 mvp_matrix;
  vec4 camera_position;
};

// Data unique to each vertex
struct LocalVertexData {
  vec3 position;
  vec3 normal_vector;
  vec2 tex_coords;
};
} // namespace structs
